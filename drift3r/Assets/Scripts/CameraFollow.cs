﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public bool rotationFollow = false;
    public Transform _toFollow;

    
    public float smoothTime = 0.3F;
    private Vector3 velocity = Vector3.zero;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = _toFollow.position;
        if (rotationFollow)
        {
            Vector3 targetAngle = _toFollow.transform.eulerAngles;

            Vector3 smoothedAngle = Vector3.SmoothDamp(this.transform.eulerAngles, targetAngle, ref velocity, smoothTime);
            this.transform.eulerAngles = smoothedAngle;
        }
    }
}
