﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovement : MonoBehaviour
{
    public GameObject _directionHelper;
    public GameObject _carParent;
    public Rigidbody _rb;


    public float carFakeRotator = 10;
    public float _directionY = 0;
    public float force =100f;    
    public float carTurnSpeed=1;
    public List<GameObject> _wheels;
    
    public ParticleSystem particle;
    public Joystick variableJoystick;

    private float input =0;

    private float smoothTime = 0.3f;
    private float tempInput;
    private float smoothInput;

    

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        input = 0;
        //HANDLING AND INPUT
        
        if (Input.GetKey(KeyCode.A))
        {
            input = -1;
        }
        if (Input.GetKey(KeyCode.D))
        {
            input = 1;
        }

        if (Input.touchCount > 0)
        {
            var touch = Input.GetTouch(0);
            if (touch.position.x < Screen.width / 2)
            {
                input = -1;
            }
            else if (touch.position.x > Screen.width / 2)
            {
                input = 1;
            }
        }
        float joystickInput = Input.GetAxis("Horizontal");

        if( input == 0)
        {
            input = joystickInput;
        }
        //input = Input.GetAxis("Horizontal"); //variableJoystick.Horizontal;
        smoothInput = Mathf.SmoothDamp(smoothInput, input, ref tempInput, smoothTime);

        RotateCar(smoothInput);
        //
        float turningModifier = 1 - (Mathf.Abs(smoothInput)/2);
        //MOVEMENT
        _directionHelper.transform.eulerAngles = new Vector3(0, _directionY, 0);
        _rb.AddForce(_directionHelper.transform.forward * force*_rb.mass * turningModifier);

        


        //FAKE ROTATIONS
        _carParent.transform.localEulerAngles = new Vector3(0, carFakeRotator * smoothInput, 0);


        //PARTICLES
        float colorAlpha = Mathf.Abs(smoothInput);
        Color color = Color.white;
        color.a = colorAlpha;
        particle.startColor = color;
    }

    private void RotateCar(float value)
    {

        _directionY+=  value * Time.deltaTime * carTurnSpeed;
        foreach (var wheel in _wheels)
        {
            wheel.transform.localEulerAngles = new Vector3(0, 0, -value * carFakeRotator);
        }
        
    }
}
